﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace MilestoneProject
{
    public partial class mainPage : Form
    {
        public mainPage()
        {
            InitializeComponent();
        }//ends public home
   
        private void AddBtn_Click(object sender, EventArgs e)
        {
            string nameOfItem;
            string description; 
            string manufacturer;
            int ID; 
            int quantity;
            double price;

            //This if is checking to make sure that all text boxs have some form of text within them. 
            if (!String.IsNullOrEmpty(nameTxtBx.Text) && !String.IsNullOrEmpty(IDTxtBx.Text) && !String.IsNullOrEmpty(descriptionTxtBx.Text) 
                && !String.IsNullOrEmpty(quantityTxtBx.Text) && !String.IsNullOrEmpty(manufacturerTxtBx.Text) && !String.IsNullOrEmpty(pricePerItemTxtBx.Text))
            {
                nameOfItem = nameTxtBx.Text;
                description = descriptionTxtBx.Text;
                manufacturer = manufacturerTxtBx.Text;

                //If you made it to this if, it is making sure the numbers that are used are valid inputs
                if (int.TryParse(IDTxtBx.Text, out ID) && int.TryParse(quantityTxtBx.Text, out quantity) && double.TryParse(pricePerItemTxtBx.Text, out price)) {
                    InventoryItem item = new InventoryItem(nameOfItem, ID, description, quantity, manufacturer, price);
                    addToTable(item);
                }//ends try parse if check
            }//ends text box not null check, later this could be expanded to white space characters. 

        }//ends add btn.

        //This method is for unit testing to see that information was stored to the object and can be retrieved while updating the page. 
        private void addToTable(InventoryItem item)
        {
            int n = dataGridView1.Rows.Add();
            dataGridView1.Rows[n].Cells[0].Value = item.itemName;
            dataGridView1.Rows[n].Cells[1].Value = item.itemID;
            dataGridView1.Rows[n].Cells[2].Value = item.itemDescription;
            dataGridView1.Rows[n].Cells[3].Value = item.itemQuantity;
            dataGridView1.Rows[n].Cells[4].Value = item.itemManufacturer;
            dataGridView1.Rows[n].Cells[5].Value = item.pricePerItem;
        }//ends add to table



        //pre-plan ignore. was going to use this for unit testing but decided not to. 
        private void EditBtn_Click(object sender, EventArgs e)
        {
            
        }//ends edit button

    }//ends class Home
}//ends namespace
