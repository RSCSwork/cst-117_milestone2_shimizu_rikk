﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MilestoneProject
{
    class InventoryItem
    {
        //these variables are "public" but only get is made public.  the set is still private as specified by the name.
        //This idea was found on stack overflow.
        public string itemName { get; private set; }
        public int itemID { get; private set; }
        public string itemDescription  { get; private set; }
        public int itemQuantity { get; private set; } = 0;
        public string itemManufacturer { get; private set; }
        public double pricePerItem { get; private set; }

        /*
         * A Constructor for the Inventory item class/objects
         * @params string itemName, int itemID, string itemDescription, int itemQuantity, string itemManufacturer, double pricePerItem
         * 
         * This constructor will also serve as part of the unit testing, to prove an item can be created/added to the table.
         * Since this week the milestone is meant to start getting functionality and prove that functionality, it made sense to do a basic setting
         * of an InventoryItem and show it in the XML on the GUI.
         */
        public InventoryItem(string itemName, int itemID, string itemDescription, int itemQuantity, string itemManufacturer, double pricePerItem)
        {
            this.itemName = itemName;
            this.itemID = itemID;
            this.itemDescription = itemDescription;
            this.itemQuantity = itemQuantity;
            this.itemManufacturer = itemManufacturer;
            this.pricePerItem = pricePerItem;
        }//ends constructor

    }//ends Inventory Item class
}//ends name space
