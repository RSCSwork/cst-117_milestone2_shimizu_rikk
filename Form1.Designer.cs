﻿namespace MilestoneProject
{
    partial class mainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addBtn = new System.Windows.Forms.Button();
            this.searchBtn = new System.Windows.Forms.Button();
            this.nameTxtBx = new System.Windows.Forms.TextBox();
            this.IDTxtBx = new System.Windows.Forms.TextBox();
            this.descriptionTxtBx = new System.Windows.Forms.TextBox();
            this.quantityTxtBx = new System.Windows.Forms.TextBox();
            this.manufacturerTxtBx = new System.Windows.Forms.TextBox();
            this.nameLbl = new System.Windows.Forms.Label();
            this.IDLbl = new System.Windows.Forms.Label();
            this.descriptionLbl = new System.Windows.Forms.Label();
            this.quantityLbl = new System.Windows.Forms.Label();
            this.manufacturerLbl = new System.Windows.Forms.Label();
            this.removeBtn = new System.Windows.Forms.Button();
            this.moveToBtn = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.itemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Manufacturer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pricePerItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.moveToLbl = new System.Windows.Forms.Label();
            this.pricePerItemLbl = new System.Windows.Forms.Label();
            this.pricePerItemTxtBx = new System.Windows.Forms.TextBox();
            this.editBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(16, 320);
            this.addBtn.MinimumSize = new System.Drawing.Size(100, 50);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(100, 51);
            this.addBtn.TabIndex = 0;
            this.addBtn.Text = "Add";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // searchBtn
            // 
            this.searchBtn.Location = new System.Drawing.Point(122, 320);
            this.searchBtn.MinimumSize = new System.Drawing.Size(100, 50);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(100, 50);
            this.searchBtn.TabIndex = 1;
            this.searchBtn.Text = "Search";
            this.searchBtn.UseVisualStyleBackColor = true;
            // 
            // nameTxtBx
            // 
            this.nameTxtBx.Location = new System.Drawing.Point(160, 45);
            this.nameTxtBx.Name = "nameTxtBx";
            this.nameTxtBx.Size = new System.Drawing.Size(100, 26);
            this.nameTxtBx.TabIndex = 2;
            // 
            // IDTxtBx
            // 
            this.IDTxtBx.Location = new System.Drawing.Point(160, 93);
            this.IDTxtBx.Name = "IDTxtBx";
            this.IDTxtBx.Size = new System.Drawing.Size(100, 26);
            this.IDTxtBx.TabIndex = 3;
            // 
            // descriptionTxtBx
            // 
            this.descriptionTxtBx.Location = new System.Drawing.Point(160, 137);
            this.descriptionTxtBx.Name = "descriptionTxtBx";
            this.descriptionTxtBx.Size = new System.Drawing.Size(100, 26);
            this.descriptionTxtBx.TabIndex = 4;
            // 
            // quantityTxtBx
            // 
            this.quantityTxtBx.Location = new System.Drawing.Point(160, 186);
            this.quantityTxtBx.Name = "quantityTxtBx";
            this.quantityTxtBx.Size = new System.Drawing.Size(100, 26);
            this.quantityTxtBx.TabIndex = 5;
            // 
            // manufacturerTxtBx
            // 
            this.manufacturerTxtBx.Location = new System.Drawing.Point(160, 234);
            this.manufacturerTxtBx.Name = "manufacturerTxtBx";
            this.manufacturerTxtBx.Size = new System.Drawing.Size(100, 26);
            this.manufacturerTxtBx.TabIndex = 6;
            // 
            // nameLbl
            // 
            this.nameLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nameLbl.AutoSize = true;
            this.nameLbl.Location = new System.Drawing.Point(12, 45);
            this.nameLbl.MinimumSize = new System.Drawing.Size(100, 25);
            this.nameLbl.Name = "nameLbl";
            this.nameLbl.Size = new System.Drawing.Size(100, 25);
            this.nameLbl.TabIndex = 7;
            this.nameLbl.Text = "Name";
            // 
            // IDLbl
            // 
            this.IDLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IDLbl.AutoSize = true;
            this.IDLbl.Location = new System.Drawing.Point(12, 93);
            this.IDLbl.MinimumSize = new System.Drawing.Size(100, 25);
            this.IDLbl.Name = "IDLbl";
            this.IDLbl.Size = new System.Drawing.Size(100, 25);
            this.IDLbl.TabIndex = 8;
            this.IDLbl.Text = "ID";
            // 
            // descriptionLbl
            // 
            this.descriptionLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.descriptionLbl.AutoSize = true;
            this.descriptionLbl.Location = new System.Drawing.Point(12, 137);
            this.descriptionLbl.MinimumSize = new System.Drawing.Size(100, 25);
            this.descriptionLbl.Name = "descriptionLbl";
            this.descriptionLbl.Size = new System.Drawing.Size(100, 25);
            this.descriptionLbl.TabIndex = 9;
            this.descriptionLbl.Text = "Description";
            // 
            // quantityLbl
            // 
            this.quantityLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.quantityLbl.AutoSize = true;
            this.quantityLbl.Location = new System.Drawing.Point(12, 186);
            this.quantityLbl.MinimumSize = new System.Drawing.Size(100, 25);
            this.quantityLbl.Name = "quantityLbl";
            this.quantityLbl.Size = new System.Drawing.Size(100, 25);
            this.quantityLbl.TabIndex = 10;
            this.quantityLbl.Text = "Quantity";
            // 
            // manufacturerLbl
            // 
            this.manufacturerLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.manufacturerLbl.AutoSize = true;
            this.manufacturerLbl.Location = new System.Drawing.Point(12, 234);
            this.manufacturerLbl.MinimumSize = new System.Drawing.Size(100, 25);
            this.manufacturerLbl.Name = "manufacturerLbl";
            this.manufacturerLbl.Size = new System.Drawing.Size(104, 25);
            this.manufacturerLbl.TabIndex = 11;
            this.manufacturerLbl.Text = "Manufacturer";
            // 
            // removeBtn
            // 
            this.removeBtn.Location = new System.Drawing.Point(16, 394);
            this.removeBtn.MinimumSize = new System.Drawing.Size(100, 50);
            this.removeBtn.Name = "removeBtn";
            this.removeBtn.Size = new System.Drawing.Size(100, 50);
            this.removeBtn.TabIndex = 12;
            this.removeBtn.Text = "Remove";
            this.removeBtn.UseVisualStyleBackColor = true;
            // 
            // moveToBtn
            // 
            this.moveToBtn.Location = new System.Drawing.Point(86, 494);
            this.moveToBtn.MinimumSize = new System.Drawing.Size(100, 25);
            this.moveToBtn.Name = "moveToBtn";
            this.moveToBtn.Size = new System.Drawing.Size(100, 26);
            this.moveToBtn.TabIndex = 13;
            this.moveToBtn.Text = "Move To";
            this.moveToBtn.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.itemName,
            this.ID,
            this.Description,
            this.Quantity,
            this.Manufacturer,
            this.pricePerItem});
            this.dataGridView1.Location = new System.Drawing.Point(306, 45);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 28;
            this.dataGridView1.Size = new System.Drawing.Size(684, 610);
            this.dataGridView1.TabIndex = 14;
            // 
            // itemName
            // 
            this.itemName.HeaderText = "Name";
            this.itemName.Name = "itemName";
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            // 
            // Description
            // 
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            // 
            // Quantity
            // 
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.Name = "Quantity";
            // 
            // Manufacturer
            // 
            this.Manufacturer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Manufacturer.HeaderText = "Manufacturer";
            this.Manufacturer.Name = "Manufacturer";
            this.Manufacturer.Width = 140;
            // 
            // pricePerItem
            // 
            this.pricePerItem.HeaderText = "Price per Item";
            this.pricePerItem.Name = "pricePerItem";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(192, 494);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(68, 26);
            this.textBox1.TabIndex = 15;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(16, 494);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(68, 26);
            this.textBox2.TabIndex = 16;
            // 
            // moveToLbl
            // 
            this.moveToLbl.AutoSize = true;
            this.moveToLbl.Location = new System.Drawing.Point(12, 471);
            this.moveToLbl.MinimumSize = new System.Drawing.Size(250, 0);
            this.moveToLbl.Name = "moveToLbl";
            this.moveToLbl.Size = new System.Drawing.Size(250, 20);
            this.moveToLbl.TabIndex = 17;
            this.moveToLbl.Text = "Move from one ID to another";
            this.moveToLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pricePerItemLbl
            // 
            this.pricePerItemLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pricePerItemLbl.AutoSize = true;
            this.pricePerItemLbl.Location = new System.Drawing.Point(12, 275);
            this.pricePerItemLbl.MinimumSize = new System.Drawing.Size(100, 25);
            this.pricePerItemLbl.Name = "pricePerItemLbl";
            this.pricePerItemLbl.Size = new System.Drawing.Size(107, 25);
            this.pricePerItemLbl.TabIndex = 18;
            this.pricePerItemLbl.Text = "Price per Item";
            // 
            // pricePerItemTxtBx
            // 
            this.pricePerItemTxtBx.Location = new System.Drawing.Point(160, 275);
            this.pricePerItemTxtBx.Name = "pricePerItemTxtBx";
            this.pricePerItemTxtBx.Size = new System.Drawing.Size(100, 26);
            this.pricePerItemTxtBx.TabIndex = 19;
            // 
            // editBtn
            // 
            this.editBtn.Location = new System.Drawing.Point(122, 394);
            this.editBtn.MinimumSize = new System.Drawing.Size(100, 50);
            this.editBtn.Name = "editBtn";
            this.editBtn.Size = new System.Drawing.Size(100, 50);
            this.editBtn.TabIndex = 20;
            this.editBtn.Text = "Edit";
            this.editBtn.UseVisualStyleBackColor = true;
            this.editBtn.Click += new System.EventHandler(this.EditBtn_Click);
            // 
            // mainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 680);
            this.Controls.Add(this.editBtn);
            this.Controls.Add(this.pricePerItemTxtBx);
            this.Controls.Add(this.pricePerItemLbl);
            this.Controls.Add(this.moveToLbl);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.moveToBtn);
            this.Controls.Add(this.removeBtn);
            this.Controls.Add(this.manufacturerLbl);
            this.Controls.Add(this.quantityLbl);
            this.Controls.Add(this.descriptionLbl);
            this.Controls.Add(this.IDLbl);
            this.Controls.Add(this.nameLbl);
            this.Controls.Add(this.manufacturerTxtBx);
            this.Controls.Add(this.quantityTxtBx);
            this.Controls.Add(this.descriptionTxtBx);
            this.Controls.Add(this.IDTxtBx);
            this.Controls.Add(this.nameTxtBx);
            this.Controls.Add(this.searchBtn);
            this.Controls.Add(this.addBtn);
            this.Name = "mainPage";
            this.Text = "Inventory Management System";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.TextBox nameTxtBx;
        private System.Windows.Forms.TextBox IDTxtBx;
        private System.Windows.Forms.TextBox descriptionTxtBx;
        private System.Windows.Forms.TextBox quantityTxtBx;
        private System.Windows.Forms.TextBox manufacturerTxtBx;
        private System.Windows.Forms.Label nameLbl;
        private System.Windows.Forms.Label IDLbl;
        private System.Windows.Forms.Label descriptionLbl;
        private System.Windows.Forms.Label quantityLbl;
        private System.Windows.Forms.Label manufacturerLbl;
        private System.Windows.Forms.Button removeBtn;
        private System.Windows.Forms.Button moveToBtn;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label moveToLbl;
        private System.Windows.Forms.Label pricePerItemLbl;
        private System.Windows.Forms.TextBox pricePerItemTxtBx;
        private System.Windows.Forms.Button editBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Manufacturer;
        private System.Windows.Forms.DataGridViewTextBoxColumn pricePerItem;
    }
}

